import tensorflow as tf
import torch
from torch.autograd import Variable
import onnx
from os import path
from onnx_tf.backend import prepare


def transform(trained_pytorch_model, name, input_shape, model_folder):
    if path.exists(model_folder+"/"+name):
        tf.saved_model.load(model_folder+"/"+name)
    dummy_input = Variable(torch.randn(1, *input_shape))
    if not path.exists(model_folder+"/"+name+".onnx"):
        torch.onnx.export(trained_pytorch_model, tuple([dummy_input]), model_folder+"/"+name+".onnx",
                          operator_export_type=torch.onnx.OperatorExportTypes.ONNX_ATEN_FALLBACK)
    onnx_model = onnx.load(model_folder+"/"+name+".onnx")
    tensorflow_model = prepare(onnx_model, strict=False)
    tf.saved_model.save(tensorflow_model, model_folder+"/"+name)
