from torchvision import models
from pytorch_to_tensorflow import transform

pytorch_alexnet = models.alexnet(pretrained=True)
tf_alexnet = transform(pytorch_alexnet, "alexnet", (3, 277, 277), "../models")
