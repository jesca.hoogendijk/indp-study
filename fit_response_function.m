function p = fit_response_function(responses,stimX, stimY, stimulus)
%In this call, 'responses' is the set of responses of a single node to all
%stimulus states.
%stimX and stimY are the stimulus location in X and Y coordinates that produced each response.
%This can be in pixels for visual field mapping stimuli.
%For numerosity stimuli, stimX should log(numerosity) of the presented
%display and stimY should be a set of zeros of the same size as stimX.

%Stimulus is a strange input. It describes which stimulus locations are
%activated during each response, so this also works if multiple stimulus
%pixels are on together. This is an advanced step, but potentially
%very useful later as our experiments do this. 

%If only one pixels is activated and we are just stepping through the list in stimX and stimY,
%this can be an identity matrix. So...
if ~exist(stimulus, 'var') || isempty(stimulus) %If this input is absent.
    stimulus=eye(length(stimX));    %Matlab's identity matrix function.
end


%It's also useful to run this in parallel for many node's responses. But
%it's much more complex (as you'll see in the loop).
%Let's make an option to turn this on and off:
if min(size(responses))>1   %If the response passed in is 2D, so for more than 1 node
    parallelMadness=1;
else
    parallelMadness=0;
end



%Which parameters we want to test our model against.
modelX=0:0.25:100;   %From zero to 100 in steps of 0.25. Generally we use a 
%step size around 1/4 of the step between stimulus states.
%This maximum (here 100) should a little higher the maximum in your
%stimulus set, the minimum should be a little lower than the minimum in
%your stimulus set. This lets you find response functions that are only
%increasing or only decreasing across the stimulus area, which don't have a
%preferred stimulus state within the set we tested.
%For numerosity response models, this should be in log space, and not
%include zero as zero has no log. Like:
%log(0.75:0.25:10)
modelY=0:0.25:100;  %Same principle. For numerosity, this should be a single zero.
modelSigma=0.25:0.25:max(modelX);   %For the tuning width, the Gaussian function's standard deviation.

%Initialise 'the best correlation so far' to zero.
bestR2=0;
bestX=0;
bestY=0;
bestSigma=0;

%Things need to work differently for parallel
if parallelMadness==1
    varResp = var(responses);   %The variance of the responses, which we are trying to capture with our model
    err = varY;     %Running total of the lowest residual, initialised to the response varaince
    o = ones(size(responses,1),1);  %Useful later
    p = zeros(4,size(responses,2)); %Somewhere to store our best parameters: x,y, sigma, R2.
end

for thisModelX = modelX %Loop through every value of modelX
    for thisModelY = modelY %Loop through every value of modelY within each value of modelX
        for thisModelSigma = modelSigma %Loop through every value of modelSigma within these loops so every combination is tested.
            %The Gaussian response function defined by these parameters,
            %evaluted at each stimulus state.
            g = exp(((stimX-thisModelX).^2+(stimY-thisModelY).^2)./(-2*thisModelSigma.^2));
            pred   = stimulus*g(:); %The activation of this Gaussian by the stimulus
            
            %Determine the correlation between this model Gaussian's prediction and the
            %measured responses. 
            %1) This is the simple way, for one pixel activated and each
            %node's response tested separately.
            if parallelMadness==0
                tmp=corrcoef(pred, responses); %Correlation between the Gaussian and the measured response
                r2=tmp(1,2);    %This is just because matlab outputs correlation coefficients strangely.
                %If this is the best yet, keep an updated account of the best
                %parameters.
                if r2>bestR2
                    bestR2=r2;
                    bestX=thisModelX;
                    bestY=thisModelY;
                    bestSigma=thisModelSigma;
                    bestRF =g;
                    p=[bestX, bestY, bestSigma, bestR2];
                end
            else
                
                %2) The complex way, but potentially much faster by testing all
                %nodes in parallel. If you want to try this, first make sure
                %the result is the same as the first way.
                X = [pred(:) o];    %The two terms here are the prediction and a fixed component to capture a non-zero intercept.
                scale = pinv(X)*responses;  %For each response set passed in, linearly solve the slope and intercept of the best response scaling.
                U = var(responses - X*scale);   %Calculate the variance of the residual (unexplained by the model)
                r2= 1 - (U./varResp);    %The proportion of the the response varaince that IS explained by the model
                
                keep = U<err;   %Find which responses are best explained by this model (of the models tested so far)
                p(1,keep) = thisModelX;   %Set the best parameters yet for those responses best explained by this model
                p(2,keep) = thisModelY;
                p(3,keep) = thisModelSigma;
                p(4,keep) = r2(keep);   %Because r2 was calculated for all responses separately.
                err(keep) = U(keep);    %Update the best variance of the residual for future comparisons
            end
        end
    end
end

