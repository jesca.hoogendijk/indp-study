from code_analysis import *
from scipy.stats import f_oneway

verbose = True
storage_manager = StorageManager("/media/harveylab/data3/data_jesca")
mem_lim_gb = 25
Plot.mem_lim_gb = mem_lim_gb


class PredictionTableVariables:
    def __init__(self, table, eval_table, partitioned, log, step, shape, number_of_repetitions, feedforward_only):
        self.table = table
        self.eval_table = eval_table
        self.partitioned = partitioned
        self.log = log
        self.step = step
        self.shape = shape
        self.number_of_repetitions = number_of_repetitions
        self._p = None
        self.result_table = f"{table}_predictions_step{step[0]}_log{log}"
        self.feedforward_only = feedforward_only

    @property
    def p(self):
        if self._p is None:
            self._p = FittingManager.init_result_array(self.step, self.shape, linearise_s=self.log)
        return self._p


class PredictionTables:
    PRFL = PredictionTableVariables('PredNet_PRF', 'PredNet_PRF', True, True, (8, 8, 0.05), (128, 160, 6), 1, False)
    PRF = PredictionTableVariables('PredNet_PRF', 'PredNet_PRF', True, False, (8, 8, 2), (128, 160, 70), 1, False)
    PRFF = PredictionTableVariables('PredNet_PRF_feedforward_fitting_predictions_evaluation_step8', 'PredNet_PRF_feedforward_fitting_predictions_evaluation_step8', True, False, (8, 8, 2), (128, 160, 70), 1, True)
    NVL = PredictionTableVariables('PredNet_Numerosity_variants', 'PredNet_Numerosity_variants', True, True, (1, 1, 0.05), (20, 1, 6), 30, False)
    NV = PredictionTableVariables('PredNet_Numerosity_variants', 'PredNet_Numerosity_variants', True, False, (1, 1, 1), (20, 1, 20), 30, False)
    NVFL = PredictionTableVariables('PredNet_Numerosity_variants_feedforward', 'PredNet_Numerosity_variants_feedforward', True, True, (1, 1, 0.05), (20, 1, 6), 30, False)
    NVF = PredictionTableVariables('PredNet_Numerosity_variants_feedforward', 'PredNet_Numerosity_variants_feedforward', True, False, (1, 1, 1), (20, 1, 20), 30, False)
    NVML = PredictionTableVariables('PredNet_Numerosity_variants_mean', 'PredNet_Numerosity_variants', False, True, (1, 1, 0.05), (20, 1, 6), 30, False)
    NVM = PredictionTableVariables('PredNet_Numerosity_variants_mean', 'PredNet_Numerosity_variants', False, False, (1, 1, 1), (20, 1, 20), 30, False)
    NML = PredictionTableVariables('PredNet_Numerosity_mean', 'PredNet_Numerosity', False, True, (1, 1, 0.05), (20, 1, 6), 100, False)
    NM = PredictionTableVariables('PredNet_Numerosity_mean', 'PredNet_Numerosity', False, False, (1, 1, 1), (20, 1, 20), 100, False)


def load_dataset(ptv: PredictionTableVariables, nodes):
    tbl = storage_manager.open_table(ptv.table, verbose)

    # limit = (Plot.mem_lim_gb * 10**6) / tbl.dtype.itemsize
    # if isinstance(nodes, tuple):
    #     nodes = to_1d_array(nodes[0], 0), to_1d_array(nodes[1], 1)
    # if isinstance(nodes, np.ndarray) or isinstance(nodes, slice):
    #     nodes = to_1d_array(nodes, 0), to_1d_array(slice(None), 1)
    # total_size = len(nodes[0]) * len(nodes[1])
    # n_batches = int(np.ceil(total_size / limit))
    # goodness_of_fits = []
    # fit_descriptions = []
    # for i in range(0, n_batches):
    #     if ptv.partitioned:
    #         fit_selection = tbl[nodes[0], nodes[1][int(i*np.ceil(len(nodes[1])/n_batches)):int((i+1)*np.ceil(len(nodes[1])/n_batches))]]
    #         fit_selection = fit_selection.T
    #     else:
    #         fit_selection = tbl[nodes[0][int(i*np.ceil(len(nodes[1])/n_batches)):int(i+1*np.ceil(len(nodes[1])/n_batches))], nodes[1]]
    #     goodness_of_fits.append(np.amax(fit_selection, axis=1))
    #     ce_goodness_of_fits = goodness_of_fits[i]
    #     for n in range(len(ce_goodness_of_fits)):
    #         try:
    #             ce_fit_description = np.where(fit_selection[n] == ce_goodness_of_fits[n])[0]
    #             fit_descriptions.append(ce_fit_description[0])
    #         except IndexError:
    #             fit_descriptions.append(0)
    # goodness_of_fits = np.concatenate(goodness_of_fits)
    # pref_x = np.zeros(goodness_of_fits.shape[0])
    # for i in range(0, pref_x.shape[0]):
    #     pref_x[i] = ptv.p[fit_descriptions[i], 0]
    # pref_y = np.zeros(goodness_of_fits.shape[0])
    # for i in range(0, goodness_of_fits.shape[0]):
    #     pref_y[i] = ptv.p[fit_descriptions[i], 1]
    # pref_s = np.zeros(goodness_of_fits.shape[0])
    # for i in range(0, goodness_of_fits.shape[0]):
    #     pref_s[i] = ptv.p[fit_descriptions[i], 2]
    # return goodness_of_fits, pref_x, pref_y, pref_s

    if isinstance(nodes, np.ndarray) and nodes.dtype is np.dtype('O'):
        nodes = np.concatenate(nodes)
    best_fit_tbl = tbl.calculate_best_fits(ptv.p)
    goodness_of_fits, pref_x, pref_y, pref_s = best_fit_tbl[:, nodes]
    return goodness_of_fits, pref_x, pref_y, pref_s


def get_slices(ptv: PredictionTableVariables, layer, part=-1):
    tbl = storage_manager.open_table(ptv.table, verbose)
    slices = []
    for i in range(4):
        slices.append(Prednet.get_network_combined_layer_indices(tbl, i, ptv.feedforward_only))
    for j in range(len(slices)):
        slices[j] = (slice(None), slices[j]) if ptv.partitioned else (slices[j], slice(None))
    selection = slices[layer] if part == -1 else (slice(None), slices[layer][1][part])
    return selection


def run_anova(variables: list):
    result = f_oneway(*tuple(variables))
    print(result)


def verify_fitting():
    expected_variables = np.random.randint(1, 16, (100, 3))
    expected = np.zeros(expected_variables.shape[0])
    expected[:] = expected_variables[:, 0] + expected_variables[:, 0]
    fm = FittingManager(storage_manager)
    predicted_variables = fm.test_response_fitting(expected_variables, (16, 1, 16), (1, 1, 1), False)
    predicted = np.zeros(predicted_variables.shape[0])
    predicted[:] = predicted_variables[:, 0] + predicted_variables[:, 0]
    run_anova([expected, predicted])


def well_fit_on_input_channel():
    expected = np.zeros((128, 160))
    for x in range(0, 160, 8):
        expected[x:x+8] = x
    ptv = PredictionTables.PRFF
    nodes = get_slices(ptv, 0, 0)
    size_of_channel = 128*160
    nodes = nodes[:size_of_channel]
    goodness_of_fits, pref_x, pref_y, pref_s = load_dataset(ptv, nodes)
    run_anova([expected.reshape(-1), pref_x])


def sigma_across_layers():
    ptv = PredictionTables.PRFF
    for layer in range(4):
        nodes = get_slices(ptv, layer)
        variables = []
        for i in range(3 if layer < 3 else 2):
            goodness_of_fits, pref_x, pref_y, pref_s = load_dataset(ptv, nodes[i])
            variables.append(pref_s)
        run_anova(variables)


def numerosity_log_gaussian():
    ptv_nvf = PredictionTables.NVF
    ptv_nvfl = PredictionTables.NVFL
    variables = []
    nodes_nvf = get_slices(ptv_nvf, slice(0, 4))
    goodness_of_fits_nvf, _, _, _ = load_dataset(ptv_nvf, nodes_nvf)
    variables.append(goodness_of_fits_nvf)
    nodes_nvfl = get_slices(ptv_nvfl, slice(0, 4))
    goodness_of_fits_nvfl, _, _, _ = load_dataset(ptv_nvfl, nodes_nvfl)
    variables.append(goodness_of_fits_nvfl)
    run_anova(variables)


def moving_sigma_across_layers():
    ptv = PredictionTables.NVFL
    variables = []
    for i in range(4):
        nodes = get_slices(ptv, i)
        goodness_of_fits, pref_x, pref_y, pref_s = load_dataset(ptv, nodes)
        variables.append(pref_s)
    run_anova(variables)


verify_fitting()
