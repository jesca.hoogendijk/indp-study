import sys
from dotGenerator import generateDotImage

### Example:
# > python dotGen.py nDots eqArea imgLenPx ['area'|'size'|'circumference'] folder iteration
# > python ./dotGeneration/dotGen.py 3 1 100 area ./images 1
### Args:
# ndots: number of dots to generate
# eqArea: ??
# imgLenPx: Size of the image
# ['area'|'size'|'circumference']: way to calculate dot relation
# folder: Folder to store images
# iteration: Appends to filename of the image in case of multiple images being generated

# any arg check
if len(sys.argv) < 7:
    sys.exit("Too few parameters\n" \
        "Usage: dotGen.py nDots eqArea imgLenPx ['area'|'size'|'circumference'] folder iteration\n" \
        "Example: ./dotGen.py 3 1 150 area ./images 1")

ndots, eqArea, imgLenPx, calculationFunction, folder, iteration = tuple(sys.argv[1:])
generateDotImage(eqArea, ndots, imgLenPx, calculationFunction, folder, iteration)
