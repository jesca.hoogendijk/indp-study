import sys
import math
import numpy as np
from enum import Enum
from newDotPattern import newDotPattern
from drawImage import drawImage


### Args:
# ndots: number of dots to generate
# eqArea: ??
# imgLenPx: Size of the image
# calculationFunction: way to calculate dot relation
# folder: Folder to store images
# iteration: Appends to filename of the image in case of multiple images being generated
def generateDotImage(eqArea, ndots, imgLenPx, calculationFunction, folder, iteration):
    ### Parameters
    WinPos                  = Enum('WinPos', 'area size circumference')
    params_experiment       = ""
    params_conditionOrder   = ""
    params_equalArea        = int(eqArea) # 0 | 1 | 2
    ndots                   = int(ndots)
    imgLenPx                = int(imgLenPx)
    dotSize                 = -1
    dotSizeIn               = -1
    recheckDist             = -1
    dotColors               = np.array([[0, 0, 0], [0, 0, 0]])
    folder = folder
    iteration = iteration


    # arg1 check
    if ndots <= 0:
        sys.exit("arg1 (nDots) must be > 0")
    # arg2 check
    if params_equalArea< 0 or params_equalArea > 2:
        sys.exit("arg2 (eqArea) must be [0|1|2]")
    # arg3 check
    if imgLenPx < 0:
        sys.exit("arg3 (imgLenPx) must be > 0")
    # arg4 check
    if calculationFunction == 'area':
        params_conditionOrder   = WinPos.area
    elif calculationFunction == 'size':
        params_conditionOrder   = WinPos.size
    elif calculationFunction == 'circumference':
        params_conditionOrder   = WinPos.circumference
    else:
        sys.exit("arg4 must be ['area'|'size'|'circumference']")
    # arg compatibilty check
    if dotSize >= imgLenPx:
        sys.exit("dot size cannot be greater or equal to image length in pxs")

    ### Get Experiment Name
    if params_conditionOrder == WinPos.area:
        params_experiment   = 'Dots Area pRF full blanks TR=1.5, nTRs=3'
        params_equalArea    = 1
        dotSizeIn           = 3*(7/2)**2*math.pi
        dotColors           = np.array([[0, 0, 0], [0, 0, 0]])
    elif params_conditionOrder == WinPos.size:
        params_experiment   = 'Dots Size pRF full blanks TR=1.5, nTRs=3'
        params_equalArea    = 0
        dotSize             = 7
        dotColors           = np.array([[0, 0, 0], [0, 0, 0]])
    elif params_conditionOrder == WinPos.circumference:
        params_experiment   = 'Dots Circumference pRF full blanks TR=1.5, nTRs=3'
        params_equalArea    = 2
        dotSizeIn           = 19*math.pi*3
        dotColors = np.array([[0, 0, 0], [0, 0, 0]])

    ### Get Recheck Distribution Based on ndots
    if params_equalArea == 1:
        dotSize = (2*(math.sqrt((dotSizeIn/ndots)/math.pi)))
        if ndots == 2:
            recheckDist = 5
        elif ndots == 3:
            recheckDist = 5
        elif ndots == 4:
            recheckDist = 4.8
        elif ndots == 5:
            recheckDist = 4.5
        elif ndots == 6:
            recheckDist = 4.2
        elif ndots == 7:
            recheckDist = 4
        else:
            recheckDist = 3
    elif params_equalArea == 2:
        dotSize = dotSizeIn/ndots/math.pi
        if ndots == 2:
            recheckDist = 1.15
        elif ndots == 3:
            recheckDist = 1.5
        elif ndots == 4:
            recheckDist = 1.9
        elif ndots == 5:
            recheckDist = 2.1
        elif ndots == 6:
            recheckDist = 2.3
        elif ndots == 7:
            recheckDist = 2.5
        else:
            recheckDist = 3
    elif params_equalArea == 0:
        if ndots == 2:
            recheckDist = 6
        elif ndots == 3:
            recheckDist = 5
        elif ndots == 4:
            recheckDist = 4
        elif ndots == 5:
            recheckDist = 3.5
        elif ndots == 6:
            recheckDist = 3
        elif ndots == 7:
            recheckDist = 2.8
        else:
            recheckDist = 1.4

    dotGroup = newDotPattern(ndots, imgLenPx, dotSize, recheckDist)
    drawImage(imgLenPx, dotGroup, dotSize, folder, iteration)


