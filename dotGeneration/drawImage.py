from PIL import Image, ImageDraw
from pathlib import Path


def drawImage(imgWidth, dotGroup, dotSize, folder, iteration): # image is always square
    # imgWidth: height and width of png base img
    # dotGroup: tuple of x,y coords of circle ([1,39], [43,2], ...)
    # dotSize: diameter of a single dot
    
    # make a blank image first
    image   = Image.new('RGB', (imgWidth, imgWidth))
    draw    = ImageDraw.Draw(image)

    r = dotSize / 2
    for dot in dotGroup:
        draw.ellipse((dot[0]-r, dot[1]-r, dot[0]+r, dot[1]+r), fill=(255,255,255))

    # Create folder if it does not exist
    Path(folder).mkdir(parents=True, exist_ok=True)

    image.save(folder + "/image" + iteration + ".png", "PNG")