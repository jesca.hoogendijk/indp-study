import sys
from dotGenerator import generateDotImage

### Example:
# > python generateDotImageSet.py nDots eqArea imgLenPx ['area'|'size'|'circumference'] folder amount
# > python ./dotGeneration/generateDotImageSet.py 3 1 100 area ./images 1
### Args:
# ndots: number of dots to generate
# eqArea: ??
# imgLenPx: Size of the image
# ['area'|'size'|'circumference']: way to calculate dot relation
# folder: Folder to store images
# amount: Amount of images to create

# any arg check
if len(sys.argv) < 7:
    sys.exit("Too few parameters\n" \
             "Usage: generateDotImageSet.py nDots eqArea imgLenPx ['area'|'size'|'circumference'] folder amount\n" \
             "Example: ./generateDotImageSet.py 3 1 100 area ./images 1")

nDots, eqArea, imgLenPx, calculationFunction, folder, amount = tuple(sys.argv[1:])

for i in range(0, int(amount)):
    generateDotImage(eqArea, nDots, imgLenPx, calculationFunction, folder, str(i))
