import os

import tensorflow as tf

from keras.models import Model, model_from_json
from keras.layers import Input

from prednet import PredNet
from prednet.kitti_settings import *

# check correct working directory (for debugging)
if "prednet" not in os.getcwd():
    os.chdir(os.getcwd()+"/prednet")

weights_file = os.path.join(WEIGHTS_DIR, 'tensorflow_weights/prednet_kitti_weights.hdf5')
json_file = os.path.join(WEIGHTS_DIR, 'prednet_kitti_model.json')
test_file = os.path.join(DATA_DIR, 'X_test.hkl')
test_sources = os.path.join(DATA_DIR, 'sources_test.hkl')

# Load trained model
print("Loading pre-trained model")
f = open(json_file, 'r')
json_string = f.read()
f.close()
train_model = model_from_json(json_string, custom_objects={'PredNet': PredNet})
train_model.load_weights(weights_file)


def setup_pre_trained_model(output_mode, train_model, number_of_timesteps):
    print("Creating model with output_mode="+output_mode)
    layer_config = train_model.layers[1].get_config()
    layer_config['output_mode'] = output_mode  # ['prediction'|'error'|'all'|'R|A|Ahat|E' + layer]
    data_format = layer_config['data_format'] if 'data_format' in layer_config else layer_config['dim_ordering']
    test_prednet = PredNet(weights=train_model.layers[1].get_weights(), **layer_config)
    input_shape = list(train_model.layers[0].batch_input_shape[1:])
    input_shape[0] = number_of_timesteps
    inputs = Input(shape=tuple(input_shape))
    predictions = test_prednet(inputs)
    test_model = Model(inputs=inputs, outputs=predictions)
    test_model.layers[1].step_outputs = []

    return test_model


# Create testing models
test_model_prediction = setup_pre_trained_model('prediction', train_model, 1)
test_model_prediction.compile(loss='mean_absolute_error', optimizer='adam')

# Load necessary data
print("Load testing data")
from positionalDataGenerator import *
generate_data(number_of_timesteps=1, colours=3, size_x=128, size_y=160,
              amount_of_pixels=1, stride=1, folder="../position_data")
positional_images = load_dataset(number_of_timesteps=1, colours=3, size_x=128, size_y=160,
                                 amount_of_pixels=1, stride=1, range_start=500, range_end=501,
                                 folder="../position_data")

# Perform test
print("Performing tests")
batch_size = 1
experiment_data = np.array(positional_images)
experiment_data_tensor = tf.convert_to_tensor(experiment_data, np.float32)
print("Get outputs for tests")
test_model_prediction.layers[1].step_outputs = []
prediction = test_model_prediction.layers[1](experiment_data_tensor)
feature_maps = test_model_prediction.layers[1].step_outputs
filters = test_model_prediction.layers[1].get_filters()

variables_to_discover = [(40, 90, 10), (74, 85, 5)]

print("done")
