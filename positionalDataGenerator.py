import pickle
import numpy as np
from pathlib import Path


def generate_data(number_of_timesteps, colours, size_x, size_y, amount_of_pixels=1, stride=1, folder="position_data"):
    i = 0
    for y in range(0, size_y):
        for x in range(0, size_x - (amount_of_pixels - 1), stride):
            array = np.zeros((size_x, size_y))
            for j in range(0, amount_of_pixels):
                array[x + j][y] = 1
            colour_array = list()
            for q in range(0, colours):
                colour_array.append(array)
            timesteps_array = list()
            for q in range(0, number_of_timesteps):
                timesteps_array.append(np.stack(tuple(colour_array)))
            image = np.stack(tuple(timesteps_array))
            save_image(number_of_timesteps, colours, size_x, size_y, amount_of_pixels, stride, i,
                       image, folder)
            i += 1


def save_image(number_of_timesteps, colours, size_x, size_y, amount_of_pixels, stride, i, image, folder):
    filename = get_filename(number_of_timesteps, colours, size_x, size_y, amount_of_pixels, stride, i, folder)
    # Create folder if it does not exist
    Path(folder).mkdir(parents=True, exist_ok=True)
    with open(filename, "wb") as f:
        pickle.dump(image, f)


def get_filename(number_of_timesteps, colours, size_x, size_y, amount_of_pixels, stride, i, folder):
    return folder + "/" + \
           "position_dataset" + \
           "&s=" + str(stride) + \
           "&a=" + str(amount_of_pixels) + \
           "&x=" + str(size_x) + \
           "&y=" + str(size_y) + \
           "&c=" + str(colours) + \
           "&t=" + str(number_of_timesteps) + \
           "&i=" + str(i) + \
           ".dat"


def open_image(number_of_timesteps, colours, size_x, size_y, amount_of_pixels, stride, i, folder):
    filename = get_filename(number_of_timesteps, colours, size_x, size_y, amount_of_pixels, stride, i, folder)
    try:
        with open(filename, "rb") as f:
            return pickle.load(f)
    except FileNotFoundError:
        raise FileNotFoundError("The datum file wasn't found. Make sure to generate it first using generate_data.")


def load_dataset(number_of_timesteps, colours, size_x, size_y, amount_of_pixels, stride, range_start, range_end,
                 folder="position_data"):
    dataset = []
    for i in range(range_start, range_end):
        dataset.append(open_image(number_of_timesteps, colours, size_x, size_y, amount_of_pixels, stride, i, folder))
    return dataset
