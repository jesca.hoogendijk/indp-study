from code_analysis import *

# Transform old data structure to new one
storage_manager = StorageManager("/media/harveylab/data3/data_jesca")
shape = (128, 160)
batch_size = 100
layers_d = {'A_before_pool': 3, 'Ahat': 4, 'A': 4, '_c': 4, 'c': 4, 'E': 4, 'f': 4, 'i': 4, 'o': 4, '_r': 4, 'R': 4}
row_index = list(range(0, shape[0]*shape[1]))
column_index = list()
batches = range(0, shape[0]*shape[1], batch_size)
table = 'PredNet'
tbl = storage_manager.open_table("PredNet")
start_at = tbl.nrows
if not tbl.initialised:
    start_at = 0
layer_sizes = [983040, 491520, 245760, 61440, 245760, 122880, 61440, 61440, 245760, 122880, 61440, 61440, 245760, 122880, 61440, 61440, 245760, 122880, 61440, 122880, 491520, 245760, 122880, 61440, 245760, 122880, 61440, 61440, 245760, 122880, 61440, 61440, 245760, 122880, 61440, 61440, 245760, 122880, 61440, 61440, 245760, 122880, 61440]
j = 0
for layer_type, l in layers_d.items():
    layers = range(0, l)
    for layer in layers:
        column_index.extend([layer_type + str(layer) + "e" + str(i) for i in list(range(0, layer_sizes[j]))])
        j += 1
b = 0
for batch in range(start_at, shape[0]*shape[1], batch_size):
    print(str(batch) + "/" + str(batches[-1]))
    print(int(batch/batches[-1]*100))
    start = b*batch_size
    end = start + batch_size
    if end > shape[0]*shape[1]:
        end = shape[0]*shape[1]
    rows: np.array = None
    count = {}
    for layer_type, l in layers_d.items():
        layers = range(0, l)
        for layer in layers:
            filename = "layer_type=" + layer_type + \
                       "&layer=" + str(layer) + \
                       "&batch_start=" + str(start) + \
                       "&batch_end=" + str(end)
            with open("/media/harveylab/data3/data_jesca/output/" + filename, "rb") as file:
                output = pickle.load(file)
                output_numpy = np.array([x[0].reshape(x[0].size).tolist() for x in output], dtype=np.single)
                count[layer_type + "," + str(layer)] = output_numpy.shape[1]
                if rows is None:
                    rows = output_numpy
                else:
                    rows = np.concatenate((rows, output_numpy), axis=1)
    b += 1
    storage_manager.save_results(table, rows, row_index[start:end], column_index)
