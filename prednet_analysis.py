from code_analysis import *
from prednet.kitti_settings import *

base_folder = os.getcwd()
print(base_folder)

# check correct working directory (for debugging)
if "prednet" not in base_folder:
    os.chdir(os.getcwd() + "/prednet")

weights_file = os.path.join(WEIGHTS_DIR, 'tensorflow_weights/prednet_kitti_weights.hdf5')
json_file = os.path.join(WEIGHTS_DIR, 'prednet_kitti_model.json')

batch_size = 100
shape = (128, 160)
network_input_shape = (1, 3, 128, 160)
number_of_variants = 30
verbose = True
# table = "PredNet_Numerosity"
# table = "PredNet_Numerosity_variants"
# table = "PredNet_Numerosity_variants_feedforward"
# table = "PredNet_PRF"
table = "PredNet_PRF_feedforward"
# table = "PredNet"

# Initialise managers
storage_manager = StorageManager("/media/harveylab/data3/data_jesca")
positional_input_generator = PositionalInputGenerator(1, 1, 'position_data_1_3_128_160_1_1', shape[0] * shape[1],
                                                      storage_manager, verbose=verbose)
positional_nd_input_manager = NDInputManager('position_data_1_3_128_160_1_1', network_input_shape, storage_manager,
                                             positional_input_generator, verbose=verbose)
numerosity_input_generator = NumerosityInputGenerator(int(number_of_variants/3), (0, 20), 'numerosity_asc10_0_20',
                                                      storage_manager, verbose=verbose)
numerosity_nd_input_manager = NDInputManager('numerosity_asc10_0_20', network_input_shape, storage_manager,
                                             numerosity_input_generator, verbose=verbose)
prf_input_generator = PRFInputGenerator(1, 'prf_input', storage_manager, verbose=verbose)
prf_nd_input_manager = NDInputManager('prf_input', network_input_shape, storage_manager, prf_input_generator,
                                      verbose=verbose)
nd_input_manager = prf_nd_input_manager
network = Prednet(json_file, weights_file)
output_manager = OutputManager(network, storage_manager, nd_input_manager)
fitting_manager = FittingManager(storage_manager)

# Run PredNet
network.feedforward_only = True
# output_manager.run(table, batch_size=100, resume=True)

# Average out multiple variants
# tbl = storage_manager.open_table(table)
# table += '_mean'
# for i in range(0, tbl.shape[0], number_of_variants):
#     storage_manager.save_results(table, np.mean(tbl[i:i+number_of_variants], dtype=np.float32, axis=0)[np.newaxis, ...],
#                                  row_index=[i/number_of_variants], column_index=tbl.column_index, verbose=True)
# number_of_variants = 1

# Transpose result
# storage_manager.transpose(table, verbose=verbose, batch_size=tbl.shape[1])
# table += "_T"

# Fit response functions position data
responses_tbl = storage_manager.open_table(table, verbose=verbose)
slices, layers_to_slice = Prednet.get_network_layer_indices(responses_tbl, feedforward_only=network.feedforward_only)
shape = (128, 160)
stim_x, stim_y = fitting_manager.get_stims(*shape)
step = 8
max_sigma = 8
sigma_step = 0.2
log = False
inverse_resolution = 1
fitting_results_table = f"{table}_fitting_predictions_evaluation_step{step}_sigma-step{sigma_step}"
stimulus = prf_input_generator.get_stimulus(shape)
i = 0
for _slice in slices:
    if verbose:
        print(f'{i}/{len(slices)}, {int(i/len(slices)*100)}%')
    i += 1
    responses = responses_tbl[:, _slice].T
    fitting_manager.fit_response_function(responses, stim_x, stim_y, (*shape, max_sigma),
                                          log=log,
                                          step=(step, step, sigma_step),
                                          table=fitting_results_table,
                                          parallel=True, verbose=verbose,
                                          stimulus=stimulus,
                                          indices_slice=_slice,
                                          ncols=responses_tbl.shape[1],
                                          columns=responses_tbl.column_index,
                                          dtype=np.dtype("float16"))
print()

table = "PredNet_PRF"
responses_tbl = storage_manager.open_table(table, verbose=verbose)
slices, layers_to_slice = Prednet.get_network_layer_indices(responses_tbl, feedforward_only=network.feedforward_only)
shape = (128, 160)
stim_x, stim_y = fitting_manager.get_stims(*shape)
step = 8
max_sigma = 8
sigma_step = 0.2
log = False
inverse_resolution = 1
fitting_results_table = f"{table}_fitting_predictions_evaluation_step{step}_sigma-step{sigma_step}"
stimulus = prf_input_generator.get_stimulus(shape)
i = 0
for _slice in slices:
    if verbose:
        print(f'{i}/{len(slices)}, {int(i/len(slices)*100)}%')
    i += 1
    responses = responses_tbl[:, _slice].T
    fitting_manager.fit_response_function(responses, stim_x, stim_y, (*shape, max_sigma),
                                          log=log,
                                          step=(step, step, sigma_step),
                                          table=fitting_results_table,
                                          parallel=True, verbose=verbose,
                                          stimulus=stimulus,
                                          indices_slice=_slice,
                                          ncols=responses_tbl.shape[1],
                                          columns=responses_tbl.column_index,
                                          dtype=np.dtype("float16"))
print()

# Fit results numerosity
# responses_tbl = storage_manager.open_table(table, verbose=verbose)
# print(network.feedforward_only)
# slices, layers_to_slice = Prednet.get_network_layer_indices(responses_tbl, feedforward_only=network.feedforward_only)
# max_x = 20
# stim_x = np.array(list(range(0, max_x)))
# stimulus = np.zeros((max_x*number_of_variants, max_x))
# for i in stim_x:
#     stimulus[i*number_of_variants:i*number_of_variants+number_of_variants, i] = 1
# step = 1
#
# log = True
# max_sigma = 6
# sstep = 0.05
#
# # log = False
# # max_sigma = 20
# # sstep = 1
#
# fitting_results_table = f"{table}_predictions_step{step}_log{log}"
# i = 1
# for _slice in slices:
#     if verbose:
#         print(f'{i}/{len(slices)}, {int(i/len(slices)*100)}%')
#         i += 1
#     responses = responses_tbl[:, _slice].T
#     fitting_manager.fit_response_function(responses, np.log(stim_x), np.zeros(20), (max_x, 1, max_sigma),
#                                           log=log,
#                                           step=(1, 1, sstep),
#                                           table=fitting_results_table,
#                                           parallel=True, verbose=verbose,
#                                           stimulus=stimulus,
#                                           indices_slice=_slice,
#                                           ncols=responses_tbl.shape[1],
#                                           columns=responses_tbl.column_index,
#                                           dtype=np.dtype("float16"))
# print()

# responses = responses_tbl[:].T
# fitting_manager.fit_response_function(responses, np.log(stim_x), np.zeros(20), (max_x, 1, max_sigma),
#                                       log=log,
#                                       step=(1, 1, sstep),
#                                       table=fitting_results_table,
#                                       parallel=True, verbose=verbose,
#                                       stimulus=stimulus,
#                                       ncols=responses_tbl.shape[1],
#                                       columns=responses_tbl.column_index,
#                                       dtype=np.dtype("float16"))

# Make plots
# fitting_results_tbl = storage_manager.open_table(fitting_results_table, verbose=verbose)
# fitting_results = fitting_results_tbl[::inverse_resolution]
# p = FittingManager.init_result_array(max_sigma, step, 1 / step, shape, two_d = True)
# best_sigma, nodes, max_fits_per_sigma = Plot.best_col(fitting_results, p, 2, 10)
# for node in nodes:
#     Plot.heatmap(p, fitting_results,
#                  static_column=2,
#                  static_column_value=best_sigma,
#                  x_column=0,
#                  y_column=1,
#                  node=node,
#                  xlabel='X',
#                  ylabel='Y',
#                  colour_label='Goodness of Fit',
#                  title=f'Goodness of fits for sigma {best_sigma}',
#                  x_range=(0, 128), y_range=(0, 160))
#
# indices = Prednet.get_result_indices(responses_tbl, 'A', 0, (1, 3, 128, 160), (0, 0, 0, 0), (0, 1, 0, 0))
# Plot.preferred_xys_distribution(fitting_results_tbl[indices], p[indices], 0, 'X', 'Y', 'Preferred X', 'Preferred X for channel 0 of A', (128, 160))
