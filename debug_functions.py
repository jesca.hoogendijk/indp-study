from code_analysis import *

verbose = True
save = True
storage_manager = StorageManager("/media/harveylab/data3/data_jesca")
Plot.save_fig = save
Plot.filetype = 'png'
mem_lim_gb = 25
Plot.mem_lim_gb = mem_lim_gb


class PredictionTableVariables:
    def __init__(self, table, eval_table, partitioned, log, step, shape, number_of_repetitions, feedforward_only):
        self.table = table
        self.eval_table = eval_table
        self.partitioned = partitioned
        self.log = log
        self.step = step
        self.shape = shape
        self.number_of_repetitions = number_of_repetitions
        self._p = None
        self.result_table = f"{table}_predictions_step{step[0]}_log{log}"
        self.feedforward_only = feedforward_only

    @property
    def p(self):
        if self._p is None:
            self._p = FittingManager.init_result_array(self.step, self.shape, linearise_s=self.log)
        return self._p


def get_channel_from_index(index: int, feedforward_only: bool):
    layer_sizes = {
        'i': ([(1, 3, 128, 160), (1, 48, 64, 80), (1, 96, 32, 40), (1, 192, 16, 20)]),
        'f': ([(1, 3, 128, 160), (1, 48, 64, 80), (1, 96, 32, 40), (1, 192, 16, 20)]),
        'o': ([(1, 3, 128, 160), (1, 48, 64, 80), (1, 96, 32, 40), (1, 192, 16, 20)]),
        'c': ([(1, 3, 128, 160), (1, 48, 64, 80), (1, 96, 32, 40), (1, 192, 16, 20)]),
        '_c': ([(1, 3, 128, 160), (1, 48, 64, 80), (1, 96, 32, 40), (1, 192, 16, 20)]),
        '_r': ([(1, 3, 128, 160), (1, 48, 64, 80), (1, 96, 32, 40), (1, 192, 16, 20)]),
        'Ahat': ([(1, 3, 128, 160), (1, 48, 64, 80), (1, 96, 32, 40), (1, 192, 16, 20)]),
        'A': ([(1, 3, 128, 160), (1, 48, 64, 80), (1, 96, 32, 40), (1, 192, 16, 20)]),
        'R': ([(1, 3, 128, 160), (1, 48, 64, 80), (1, 96, 32, 40), (1, 192, 16, 20)]),
        'E': ([(1, 6, 128, 160), (1, 96, 64, 80), (1, 192, 32, 40), (1, 384, 16, 20)]),
        'A_before_pool': ([(1, 48, 128, 160), (1, 96, 64, 80), (1, 192, 32, 40)])
    }
    layer_types = {
        1: 'i',
        2: 'f',
        3: 'o',
        4: 'c',
        5: '_c',
        6: '_r',
        7: 'Ahat',
        8: 'A',
        9: 'R'
    }

    if feedforward_only:
        layer_sizes = {
            'A': ([(1, 3, 128, 160), (1, 48, 64, 80), (1, 96, 32, 40), (1, 192, 16, 20)]),
            'E': ([(1, 6, 128, 160), (1, 96, 64, 80), (1, 192, 32, 40), (1, 384, 16, 20)]),
            'A_before_pool': ([(1, 48, 128, 160), (1, 96, 64, 80), (1, 192, 32, 40)])
        }
        layer_types = {
            1: 'A'
        }

    def size_from_layer(layer: (int, int, int, int)):
        return layer[0] * layer[1] * layer[2] * layer[3]

    def size_from_layer_type(layer: str):
        total = 0
        for layer in layer_sizes[layer]:
            total += size_from_layer(layer)
        return total

    layer_type_size = size_from_layer_type('A')
    e_size = size_from_layer_type('E')
    if layer_type_size * len(layer_types) < index < layer_type_size * len(layer_types) + e_size:
        layer_type = 'E'
        before = layer_type_size * len(layer_types)
    elif index > layer_type_size * len(layer_types) and index > layer_type_size * len(layer_types) + e_size:
        layer_type = 'A_before_pool'
        before = layer_type_size * len(layer_types) + e_size
    else:
        layer_type_index = np.ceil(index / layer_type_size)
        layer_type_index = 1 if layer_type_index == 0 else layer_type_index
        layer_type = layer_types[layer_type_index]
        before = layer_type_size * (layer_type_index - 1)
    this_layer_sizes = layer_sizes[layer_type]
    this_layer = 0
    layer_start = 0
    this_layer_shape = this_layer_sizes[0]
    for i in reversed(range(0, len(this_layer_sizes))):
        layer_start = before
        for j in range(0, i):
            layer_start += size_from_layer(this_layer_sizes[j])
        if index > layer_start:
            this_layer_shape = this_layer_sizes[i]
            this_layer = i
            break
    channel_size = this_layer_shape[-1] * this_layer_shape[-2]
    index_in_layer = index - layer_start
    channel_index = int(np.floor(index_in_layer / channel_size))
    after = channel_size - (index_in_layer - channel_index * channel_size)
    end = index + after
    start = end - channel_size
    return layer_type, this_layer, this_layer_shape, (0, channel_index, 0, 0), (0, channel_index, this_layer_shape[-2] - 1, this_layer_shape[-1] - 1), (int(start), int(end))


def get_interesting_channels(results: Union[np.array, Table], threshold: float, feedforward_only: bool, sort: bool = False):
    ignoring = []

    def should_ignore(node):
        for start, end in ignoring:
            if start < node < end:
                return True
        return False
    channel_identifiers = list()
    channels = list()
    j = 0
    _verbose = False
    if type(results) is Table:
        _verbose = results.verbose
        results.__verbose = False
    for i in results:
        print(f'{j}\r', end='')
        indices = np.where(i > threshold)
        if sort:
            sorted_indices = np.argsort(i)[::-1]
            sorted_i = i[sorted_indices]
            sorted_selected_indices = np.where(sorted_i > threshold)
            indices = (sorted_indices[sorted_selected_indices],)
        for index in indices[0]:
            if not should_ignore(index):
                info = get_channel_from_index(index, feedforward_only)
                channel = info[:]
                ignore = info[-1]
                ignoring.append(ignore)
                id_ = f'{channel[0]}{channel[1]}{channel[3][1]}'
                if id_ not in channel_identifiers:
                    channel_identifiers.append(id_)
                    channels.append(channel)
        j += 1
        break
    if type(results) is Table:
        results.__verbose = _verbose
    print()
    return channels


def get_channels(size: int, feedforward_only: bool):
    channel_identifiers = list()
    channels = list()
    skip_to = 0
    for index in range(size):
        if index > skip_to:
            info = get_channel_from_index(index, feedforward_only)
            channel = info[:]
            skip_to = info[-1]
            id_ = f'{channel[0]}{channel[1]}{channel[3][1]}'
            if id_ not in channel_identifiers:
                channel_identifiers.append(id_)
                channels.append(channel)
    return channels


class Graph(Enum):
    NETWORK_SCATTER_PLOT = 1
    INDIVIDUAL_NEURON = 2
    INPUTS = 3
    CHANNELS = 4
    LAYER_SCATTER_PLOT = 5
    COMBINED_LAYER = 6


# Plot individual channels
def plot_channel(pts: PredictionTableVariables, threshold, channel_index=None, title=None):
    p = pts.p
    table = pts.result_table
    feedforward_only = pts.feedforward_only
    print(table)
    responses_table = storage_manager.open_table(table, verbose=verbose)
    channels = get_interesting_channels(responses_table, threshold, feedforward_only, sort=True)
    while True:
        if channel_index is None:
            print(channels)
            which_channel = input(f'Which channel should be plotted? Type a number between 0 and {len(channels)} or stop to stop plotting.')
            if which_channel is 'stop':
                break
        else:
            which_channel = channel_index
        channel = channels[int(which_channel)]
        layer_type, layer, layer_shape, start, end, indices = channel
        image_shape = (channel[2][-2], channel[2][-1])
        column_title = 'Preferred y'
        column = 0
        Plot.title = title + "a"
        Plot.preferred_xys_distribution(responses_table, p, slice(indices[0], indices[1]), column, 'X', 'Y',
                                        f'{column_title}', '',
                                        image_shape, threshold=threshold)
        column_title = 'Preferred x'
        column = 1

        Plot.title = title + "b"
        Plot.preferred_xys_distribution(responses_table, p, slice(indices[0], indices[1]), column, 'X', 'Y',
                                        f'{column_title}', '',
                                        image_shape, threshold=threshold)
        if channel_index is not None:
            break


class Scatter(Enum):
    X_GOF = 1
    X_S = 2
    GOF_S = 3


def plot_network(pts: PredictionTableVariables, versions: List[Scatter], threshold: int, ranges: Tuple[Tuple[int, int], Tuple[int, int]], dtypes: Tuple[np.dtype, np.dtype], feedforward_only: bool):
    table = pts.table
    p = pts.p
    fitting_results_table = pts.result_table
    print(fitting_results_table)
    responses = storage_manager.open_table(fitting_results_table, verbose=verbose)
    responses_tbl = storage_manager.open_table(table, verbose=verbose)
    # responses_tbl_eval = storage_manager.open_table(eval_table, verbose=verbose)
    slices, layers_to_slice = Prednet.get_network_layer_indices(responses_tbl, feedforward_only=feedforward_only)
    # Plot the full network
    y_label = 'Preferred X'
    titles = []
    for lt, l, _, _ in layers_to_slice:
        titles.append(f'Fitted responses for layer {l} of {lt}')
    # Plot using table
    if Scatter.X_GOF in versions:
        Plot.scatter_plot_network(responses, p, 0, slices, threshold, ranges, titles, y_label, dtypes=dtypes)
    if Scatter.X_S in versions:
        Plot.scatter_plot_network(responses, p, 0, slices, threshold, ranges, titles, y_label, x_label='Preferred Sigma', col_two=2, dtypes=dtypes)
    if Scatter.GOF_S in versions:
        y_label = 'Goodness of Fit'
        Plot.scatter_plot_network(responses, p, -1, slices, threshold, ranges, titles, y_label, x_label='Preferred Sigma', col_two=2, dtypes=dtypes)


def get_actual_responses(response_tbl: Table, index: int):
    return response_tbl[:, index]


def get_index_from(_channel: tuple, loc: (int, int)):
    _layer_type, _layer, _layer_shape, _start, _end, _indices = _channel
    return int(_indices[0] + loc[0]*loc[1])


# Plot individual neuron
def plot_individual_neuron(pts: PredictionTableVariables, threshold: float, eval_tables: List[str]):
    p = pts.p
    table = pts.table
    log = pts.log
    shape = pts.shape
    number_of_repetitions = pts.number_of_repetitions
    feedforward_only = pts.feedforward_only
    responses_table = storage_manager.open_table(table, verbose=verbose)
    eval_tbls = []
    for tbl in eval_tables:
        additional_eval_tbl = storage_manager.open_table(tbl, verbose=verbose)
        eval_tbls.append(additional_eval_tbl)
    channels = get_interesting_channels(responses_table, threshold, feedforward_only)
    print(channels)
    while True:
        which_channel = input(f'Which channel should be plotted? Type a number between 0 and {len(channels)} or stop to stop plotting.')
        if which_channel is 'stop':
            break
        channel = channels[int(which_channel)]
        which_neuron = input(f'Which neuron in channel {channel} do you want to display?')
        node_index = get_index_from(channel, (int(which_neuron.split(',')[0]), int(which_neuron.split(',')[1])))
        best_fit = np.amax(responses_table[node_index])
        best_fit_p = p[np.where(responses_table[node_index] == best_fit)[0][0]]
        actual_values = []
        for tbl in eval_tbls:
            actual_values.append(get_actual_responses(tbl, node_index))
        x_index = int(input(f'X axis of the graph'))
        x_axis = best_fit_p[x_index]
        title = input(f'Title')
        x_label = input(f'x label')
        Plot.response_function(x_axis, best_fit_p[2], shape[x_index], log, title, x_label, actual_values, number_of_repetitions)


def plot_inputs():
    # Plot PRF figures
    prf_input_gen = PRFInputGenerator(1, 'prf_input', storage_manager, verbose=verbose)
    #   Vertical
    prf_input_gen.plot_image((128, 160), 10, f'Example prf input image with vertical bars')
    #   Horizontal
    prf_input_gen.plot_image((128, 160), 200, f'Example prf input image with horizontal bars')

    # Plot numerosity figures
    numerosity_input_gen = NumerosityInputGenerator(int(30 / 3), (0, 20), 'numerosity_asc10_0_20',
                                                    storage_manager, verbose=verbose)
    shape = (128, 160)
    #  Area
    calc_function = 'area'
    # 3
    numerosity_input_gen.generate_dot_image(3, shape, calc_function, True, f'Example numerosity input image with 3 dots and calculation function {calc_function}')
    # 10
    numerosity_input_gen.generate_dot_image(10, shape, calc_function, True, f'Example numerosity input image with 10 dots and calculation function {calc_function}')
    # 20
    numerosity_input_gen.generate_dot_image(20, shape, calc_function, True, f'Example numerosity input image with 20 dots and calculation function {calc_function}')
    #  Size
    calc_function = 'size'
    # 3
    numerosity_input_gen.generate_dot_image(3, shape, calc_function, True, f'Example numerosity input image with 3 dots and calculation function {calc_function}')
    # 10
    numerosity_input_gen.generate_dot_image(10, shape, calc_function, True, f'Example numerosity input image with 10 dots and calculation function {calc_function}')
    # 20
    numerosity_input_gen.generate_dot_image(20, shape, calc_function, True, f'Example numerosity input image with 20 dots and calculation function {calc_function}')
    # Circumference
    calc_function = 'circumference'
    # 3
    numerosity_input_gen.generate_dot_image(3, shape, calc_function, True, f'Example numerosity input image with 3 dots and calculation function {calc_function}')
    # 10
    numerosity_input_gen.generate_dot_image(10, shape, calc_function, True, f'Example numerosity input image with 10 dots and calculation function {calc_function}')
    # 20
    numerosity_input_gen.generate_dot_image(20, shape, calc_function, True, f'Example numerosity input image with 20 dots and calculation function {calc_function}')


class PredictionTables:
    PRFL = PredictionTableVariables('PredNet_PRF', 'PredNet_PRF', True, True, (8, 8, 0.05), (128, 160, 6), 1, False)
    PRF = PredictionTableVariables('PredNet_PRF', 'PredNet_PRF', True, False, (8, 8, 2), (128, 160, 70), 1, False)
    PRFF = PredictionTableVariables('PredNet_PRF_feedforward', 'PredNet_PRF_feedforward', True, False, (8, 8, 2), (128, 160, 70), 1, True)
    NVL = PredictionTableVariables('PredNet_Numerosity_variants', 'PredNet_Numerosity_variants', True, True, (1, 1, 0.05), (20, 1, 6), 30, False)
    NV = PredictionTableVariables('PredNet_Numerosity_variants', 'PredNet_Numerosity_variants', True, False, (1, 1, 1), (20, 1, 20), 30, False)
    NVFL = PredictionTableVariables('PredNet_Numerosity_variants_feedforward', 'PredNet_Numerosity_variants_feedforward', True, True, (1, 1, 0.05), (20, 1, 6), 30, False)
    NVF = PredictionTableVariables('PredNet_Numerosity_variants_feedforward', 'PredNet_Numerosity_variants_feedforward', True, False, (1, 1, 1), (20, 1, 20), 30, False)
    NVML = PredictionTableVariables('PredNet_Numerosity_variants_mean', 'PredNet_Numerosity_variants', False, True, (1, 1, 0.05), (20, 1, 6), 30, False)
    NVM = PredictionTableVariables('PredNet_Numerosity_variants_mean', 'PredNet_Numerosity_variants', False, False, (1, 1, 1), (20, 1, 20), 30, False)
    NML = PredictionTableVariables('PredNet_Numerosity_mean', 'PredNet_Numerosity', False, True, (1, 1, 0.05), (20, 1, 6), 100, False)
    NM = PredictionTableVariables('PredNet_Numerosity_mean', 'PredNet_Numerosity', False, False, (1, 1, 1), (20, 1, 20), 100, False)


class GraphToDo:
    variables: tuple
    graph: Graph
    prediction_table_variables: PredictionTableVariables

    def __init__(self, graph, prediction_table_variables=None, variables=None):
        self.variables = variables
        self.graph = graph
        self.prediction_table_variables = prediction_table_variables


def scatter_plot_layer(pts: PredictionTableVariables, layer_type, layer, threshold, ranges: Tuple[Tuple[int, int], Tuple[int, int]], col, col_two, title, y_label, x_label, dtypes: Tuple[np.dtype, np.dtype], feedforward_only: bool = False):
    table = pts.table
    p = pts.p
    fitting_results_table = pts.result_table
    print(fitting_results_table)
    responses = storage_manager.open_table(fitting_results_table, verbose=verbose)
    responses_tbl = storage_manager.open_table(table, verbose=verbose)
    slices, layers_to_slice = Prednet.get_network_layer_indices(responses_tbl, feedforward_only)
    index = layers_to_slice.index([item for item in layers_to_slice if item[0] == layer_type and item[1] == layer][0])
    Plot.title = title
    Plot.scatter_plot_layer(responses, p, col, slices[index], threshold, col_two, title=title, y_label=y_label, x_label=x_label, ranges=ranges, dtypes=dtypes)
    Plot.title = None


def plot_whole_network_layers(pts: PredictionTableVariables, versions: List[Scatter], threshold, ranges: Tuple[Tuple[int, int], Tuple[int, int]], titles: List, dtypes: Tuple[np.dtype, np.dtype], feedforward_only: bool = False, title=None):
    table = pts.table
    p = pts.p
    fitting_results_table = pts.result_table
    print(fitting_results_table)
    responses = storage_manager.open_table(fitting_results_table, verbose=verbose)
    responses_tbl = storage_manager.open_table(table, verbose=verbose)
    slices = []
    for i in range(4):
        slices.append(Prednet.get_network_combined_layer_indices(responses_tbl, i, feedforward_only))
    # Plot using table
    y_label = 'Preferred X'
    Plot.title = title
    if Scatter.X_GOF in versions:
        Plot.scatter_plot_network(responses, p, 0, slices, threshold, ranges, titles, y_label, dtypes=dtypes)
    if Scatter.X_S in versions:
        Plot.scatter_plot_network(responses, p, 0, slices, threshold, ranges, titles, y_label, x_label='Preferred Sigma', col_two=2, dtypes=dtypes)
    if Scatter.GOF_S in versions:
        y_label = 'Goodness of Fit'
        Plot.scatter_plot_network(responses, p, -1, slices, threshold, ranges, titles, y_label, x_label='Preferred Sigma', col_two=2, dtypes=dtypes)
    Plot.title = None


def plot_layer_combined(pts: PredictionTableVariables, layer, threshold, ranges: Tuple[Tuple[int, int], Tuple[int, int]], col, col_two, title, y_label, x_label, dtypes: Tuple[np.dtype, np.dtype], feedforward_only: bool = False, part: int = -1, file_title=None):
    table = pts.table
    p = pts.p
    fitting_results_table = pts.result_table
    print(fitting_results_table)
    responses = storage_manager.open_table(fitting_results_table, verbose=verbose)
    responses_tbl = storage_manager.open_table(table, verbose=verbose)
    slices = Prednet.get_network_combined_layer_indices(responses_tbl, layer, feedforward_only)
    selection = slices if part == -1 else slices[part]
    Plot.title = file_title
    Plot.scatter_plot_layer(responses, p, col, selection, threshold, col_two, title=title, y_label=y_label, x_label=x_label, ranges=ranges, dtypes=dtypes)
    Plot.title = None


prff = PredictionTables.PRFF
prff.result_table = 'PredNet_PRF_feedforward_fitting_predictions_evaluation_step8'
# Graph.NETWORK_SCATTER_PLOT (versions: List[Scatter], threshold: int, ranges: Tuple[Tuple[int, int], Tuple[int, int]])
# Graph.INPUTS
# Graph.CHANNELS (threshold: int)
# Graph.INDIVIDUAL_NEURON (threshold: int)
# Graph.COMBINED_LAYER (layer, threshold, ranges: Tuple[Tuple[int, int], Tuple[int, int]], col(y), col_two(x), title, y_label, x_label, dtypes: Tuple[np.dtype, np.dtype], feedforward_only: bool = False, part: int = -1)
graphs_to_do: List[GraphToDo] = [
    # Feedforward
    # Figure 1
    # GraphToDo(Graph.CHANNELS, prff, (0.2, 45, 'fig1'))
    #
    # # Figure 2 large v
    # GraphToDo(Graph.COMBINED_LAYER, prff, (1, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, 0, 'fig2a')),
    # GraphToDo(Graph.COMBINED_LAYER, prff, (1, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, 1, 'fig2b')),
    # # GraphToDo(Graph.COMBINED_LAYER, prff, (2, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, 2, 'fig2c')),
    #
    # # Figure 2 small v
    # GraphToDo(Graph.COMBINED_LAYER, prff, (1, 0, ((0, 9), None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, 0, 'fig2Sa')),
    # GraphToDo(Graph.COMBINED_LAYER, prff, (1, 0, ((0, 9), None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, 1, 'fig2Sb')),
    # # GraphToDo(Graph.COMBINED_LAYER, prff, (1, 0, (0, 9), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, 2, 'fig2Sc')),
    #
    # # Figure 3 large v
    # GraphToDo(Graph.COMBINED_LAYER, prff, (0, 0, ((0, 1), None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3a')),
    # GraphToDo(Graph.COMBINED_LAYER, prff, (1, 0, ((0, 1), None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3b')),
    # GraphToDo(Graph.COMBINED_LAYER, prff, (2, 0, ((0, 1), None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3c')),
    # GraphToDo(Graph.COMBINED_LAYER, prff, (3, 0, ((0, 1), None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3d')),
    #
    # # Figure 3 small v
    # GraphToDo(Graph.COMBINED_LAYER, prff, (0, 0, ((0, 1), (0, 9)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3Sa')),
    # GraphToDo(Graph.COMBINED_LAYER, prff, (1, 0, ((0, 1), (0, 9)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3Sb')),
    # GraphToDo(Graph.COMBINED_LAYER, prff, (2, 0, ((0, 1), (0, 9)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3Sc')),
    # GraphToDo(Graph.COMBINED_LAYER, prff, (3, 0, ((0, 1), (0, 9)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3Sd')),
    #
    # # Figure 4
    # GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVFL, (0, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig4a')),
    # GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVFL, (1, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig4b')),
    # GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVFL, (2, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig4c')),
    # GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVFL, (3, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig4d')),
    #
    # # Figure 5
    # GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVFL, (0, 0, (None, (0, 30)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig5a')),
    # GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVFL, (1, 0, (None, (0, 30)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig5b')),
    # GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVFL, (2, 0, (None, (0, 30)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig5c')),
    # GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVFL, (3, 0, (None, (0, 30)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig5d')),

    # Figure 6
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVF, (0, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig6a')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVF, (1, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig6b')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVF, (2, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig6c')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVF, (3, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig6d')),

    # Figure 7 v
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVF, (0, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig7a')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVF, (1, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig7b')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVF, (2, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig7c')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVF, (3, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig7d')),

    # Recursive
    # Figure 1
    GraphToDo(Graph.CHANNELS, PredictionTables.PRF, (0.2, 45, 'fig1R')),

    # Figure 2 large v
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (1, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, 0, 'fig2Ra')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (1, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, 1, 'fig2Rb')),
    # GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (1, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, 2, 'fig2Rc')),

    # Figure 2 small v
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (1, 0, ((0, 9), None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, 0, 'fig2RSa')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (1, 0, ((0, 9), None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, 1, 'fig2RSb')),
    # GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (1, 0, ((0, 9), None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, 2, 'fig2RSc')),

    # Figure 3 large v
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (0, 0, ((0, 1), None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3Ra')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (1, 0, ((0, 1), None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3Rb')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (2, 0, ((0, 1), None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3Rc')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (3, 0, ((0, 1), None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3Rd')),

    # Figure 3 small v
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (0, 0, ((0, 1), (0, 9)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3RSa')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (1, 0, ((0, 1), (0, 9)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3RSb')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (2, 0, ((0, 1), (0, 9)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3RSc')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.PRF, (3, 0, ((0, 1), (0, 9)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig3RSd')),

    # Figure 4
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVL, (0, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig4Ra')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVL, (1, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig4Rb')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVL, (2, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig4Rc')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVL, (3, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig4Rd')),

    # Figure 5
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVL, (0, 0, (None, (0, 30)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig5Ra')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVL, (1, 0, (None, (0, 30)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig5Rb')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVL, (2, 0, (None, (0, 30)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig5Rc')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NVL, (3, 0, (None, (0, 30)), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig5Rd')),

    # Figure 6
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NV, (0, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig6Ra')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NV, (1, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig6Rb')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NV, (2, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig6Rc')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NV, (3, 0, (None, None), -1, 0, '', 'Goodness of Fit', 'Preferred Numerosity', (np.float32, np.float32), True, -1, 'fig6Rd')),

    # Figure 7 v
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NV, (0, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig7Ra')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NV, (1, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig7Rb')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NV, (2, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig7Rc')),
    GraphToDo(Graph.COMBINED_LAYER, PredictionTables.NV, (3, 0, (None, None), -1, 2, '', 'Goodness of Fit', 'Sigma', (np.float32, np.float32), True, -1, 'fig7Rd')),
]
i = 1
for g in graphs_to_do:
    if verbose:
        print(f'{i}/{len(graphs_to_do)}, {np.ceil(i/len(graphs_to_do)*100)}%')
        i += 1
    if g.graph is Graph.NETWORK_SCATTER_PLOT:
        plot_network(g.prediction_table_variables, *g.variables)
    elif g.graph is Graph.CHANNELS:
        plot_channel(g.prediction_table_variables, *g.variables)
    elif g.graph is Graph.INDIVIDUAL_NEURON:
        plot_individual_neuron(g.prediction_table_variables, *g.variables)
    elif g.graph is Graph.INPUTS:
        plot_inputs()
    elif g.graph is Graph.LAYER_SCATTER_PLOT:
        scatter_plot_layer(g.prediction_table_variables, *g.variables)
    elif g.graph is Graph.COMBINED_LAYER:
        plot_layer_combined(g.prediction_table_variables, *g.variables)
